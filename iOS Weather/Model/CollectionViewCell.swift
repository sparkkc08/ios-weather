//
//  MyCollectionViewCell.swift
//  iOS Weather
//
//  Created by Max Mir on 6/30/19.
//  Copyright © 2019 Max Mir. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTemperature: UILabel!
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var lblAditionalInfo: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
}
