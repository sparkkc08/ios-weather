//
//  Weather.swift
//  iOS Weather
//
//  Created by Max Mir on 5/9/19.
//  Copyright © 2019 Max Mir. All rights reserved.
//

import Foundation
import CoreLocation

struct Weather {
    let summary: String
    let icon: String
    let temperatureMax: Double
    let temperatureMin: Double
    let time: Date
    let humidity: Int
    let pressure: Int

    
    enum SerealizationError: Error {
        case missing(String)
        case invalid(String, Any)
    }
    
    init(currently: [String:Any]) throws {
        guard let summary = currently["summary"] as? String else {throw SerealizationError.missing("summary id missing")}
        
        guard let icon = currently["icon"] as? String else {throw SerealizationError.missing("icon is missing")}
        
        guard let temperatureMax = currently["temperature"] as? Double else {throw SerealizationError.missing("emperature is missing")}
        
        guard let temperatureMin = currently["temperature"] as? Double else {throw SerealizationError.missing("emperature is missing")}
        
        guard let time = currently["time"] as? Double else {throw SerealizationError.missing("emperature is missing")}
        
        guard let humidity = currently["humidity"] as? Double else {throw SerealizationError.missing("emperature is missing")}
        
        guard let pressure = currently["pressure"] as? Double else {throw SerealizationError.missing("emperature is missing")}
        
        
        self.summary = summary
        self.icon = icon
        self.temperatureMax = temperatureMax
        self.temperatureMin = temperatureMin
        self.time = Date(timeIntervalSince1970: time)
        self.humidity = Int(humidity * 100)
        self.pressure = Int(pressure)
    }
    
    init(daily: [String:Any]) throws {
        guard let summary = daily["summary"] as? String else {throw SerealizationError.missing("summary id missing")}
        
        guard let icon = daily["icon"] as? String else {throw SerealizationError.missing("icon is missing")}
        
        guard let temperatureMax = daily["temperatureMax"] as? Double else {throw SerealizationError.missing("emperature is missing")}

        guard let temperatureMin = daily["temperatureMin"] as? Double else {throw SerealizationError.missing("emperature is missing")}
        
        guard let time = daily["time"] as? Double else {throw SerealizationError.missing("emperature is missing")}

        guard let humidity = daily["humidity"] as? Double else {throw SerealizationError.missing("emperature is missing")}

        guard let pressure = daily["pressure"] as? Double else {throw SerealizationError.missing("emperature is missing")}

        
        self.summary = summary
        self.icon = icon
        self.temperatureMax = temperatureMax
        self.temperatureMin = temperatureMin
        self.time = Date(timeIntervalSince1970: time)
        self.humidity = Int(humidity * 100)
        self.pressure = Int(pressure)
    }
    
    static let basePath = "https://api.darksky.net/forecast/059e3396f11e7e885aca88bdc5653925/"
    
    static func forecast(withLocation location: CLLocationCoordinate2D, completion: @escaping (Weather?, [Weather]?) -> ()) {
        let url = basePath + "\(location.latitude),\(location.longitude)" + "?exclude=minutely,hourly&units=si"
        
        let request = URLRequest(url: URL(string: url)!)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            var forecastArray: [Weather] = []
            var currentlyWeather: Weather?
            
            if let data = data {
                do {

                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        //print("Forecast json: \(json)")

                        if let currentlyForecast = json["currently"] as? [String: Any] {
                            if let currentlyWeatherObj = try? Weather(currently: currentlyForecast) {
                                currentlyWeather = currentlyWeatherObj
                            }
                        }
                        
                        if let dailyForecasts = json["daily"] as? [String: Any] {
                            if let dailyData = dailyForecasts["data"] as? [[String: Any]] {
                                for dataPoint in dailyData {
                                    if let weatherObject = try? Weather(daily: dataPoint) {
                                        forecastArray.append(weatherObject)
                                    }
                                }
                            }
                        }
                    }
                } catch {
                    print(error.localizedDescription)
                }
                
                completion(currentlyWeather, forecastArray)
            }
        
        }
        
        task.resume()
    }
}
