//
//  MainVC.swift
//  iOS Weather
//
//  Created by Max Mir on 6/30/19.
//  Copyright © 2019 Max Mir. All rights reserved.
//

import UIKit
import CoreLocation

class MainVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, CLLocationManagerDelegate {

    let cityToFind = "Kharkiv"
    @IBOutlet weak var proCollectionView: UICollectionView!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblTemperature: UILabel!
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var forecastData = [Weather]()
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        proCollectionView.delegate = self
        proCollectionView.dataSource = self
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            let status = CLLocationManager.authorizationStatus()
            if(status == .authorizedWhenInUse || status == .authorizedAlways){
                locationManager.requestLocation()
            } else if (status == .denied || status == .restricted) {
                updateWeaherForCity(cityName: cityToFind)
            }
        } else {
            updateWeaherForCity(cityName: cityToFind)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecastData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "proCell", for: indexPath) as! CollectionViewCell

        let weatherObject = forecastData[indexPath.row]
        
        cell.imgIcon.downloadedFrom(icon: weatherObject.icon)
        
        cell.lblSummary.text = weatherObject.summary
        cell.lblTemperature.text = "\(Int(weatherObject.temperatureMin))...\(Int(weatherObject.temperatureMax)) °C"

        
        cell.lblAditionalInfo.text = "\(weatherObject.pressure) hPa, \u{1D6D7} - \(weatherObject.humidity)%"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM d, yyyy"
        cell.lblDate.text = dateFormatterPrint.string(from: weatherObject.time)
        
        cell.layer.cornerRadius = 10
        return cell
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            updateWeaherForLocation(location: location)
            print("Found user's location: \(location)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        updateWeaherForCity(cityName: cityToFind)
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      if (status == .authorizedWhenInUse || status == .authorizedAlways) {
        manager.requestLocation()
      } else {
        updateWeaherForCity(cityName: cityToFind)
      }
    }
    
    func updateWeaherForCity(cityName: String) {
        CLGeocoder().geocodeAddressString(cityName) { (places: [CLPlacemark]?, error: Error?) in
            if error == nil {
                if let location = places?.first?.location {
                    self.updateWeaherForLocation(location: location)
                }
            }
        }
    }
    
    func fetchLocationCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }
    
    func updateWeaherForLocation(location: CLLocation) {
        Weather.forecast(withLocation: location.coordinate, completion: { (currentlyWeather: Weather?, results: [Weather]?) in
            if let currentlyDay = currentlyWeather {
                DispatchQueue.main.async {
                    self.lblSummary.text = currentlyDay.summary
                    self.lblTemperature.text = "\(Int(currentlyDay.temperatureMax)) °C"
                    self.imgIcon.downloadedFrom(icon: currentlyDay.icon)
                    let dateFormatterPrint = DateFormatter()
                    dateFormatterPrint.dateFormat = "MMM d, yyyy\n H:m"
                    self.lblDate.text = dateFormatterPrint.string(from: currentlyDay.time)
                }
            }
            
            if let weatherData = results {
                self.forecastData = weatherData
                
                DispatchQueue.main.async {
                    self.proCollectionView.reloadData()
                    self.activityIndicator.stopAnimating()
                }
            }
        })
        
        fetchLocationCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            print(city + ", " + country)
            self.lblCity.text = "\(city), \(country)"
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
