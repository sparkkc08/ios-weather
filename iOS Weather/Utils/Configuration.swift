//
//  Configuration.swift
//  iOS Weather
//
//  Created by Max Mir on 6/30/19.
//  Copyright © 2019 Max Mir. All rights reserved.
//

import Foundation

struct Defaults {
    
    static let Latitude: Double = 37.8267
    static let Longitude: Double = -122.423
    
}

struct API {
    
    static let APIKey = "059e3396f11e7e885aca88bdc5653925"
    static let BaseURL = URL(string: "https://api.forecast.io/forecast/")!
    
    static func forecastUrl(latitude: Double, longitude: Double) -> URL {
        return BaseURL.appendingPathComponent(APIKey)
            .appendingPathComponent("\(latitude),\(longitude)")
    }
    
}
