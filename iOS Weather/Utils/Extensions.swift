//
//  extensions.swift
//  iOS Weather
//
//  Created by Max Mir on 7/7/19.
//  Copyright © 2019 Max Mir. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(icon: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: getImageLink(icon: icon)) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
    
    func getImageLink(icon: String) -> String {
        var image: String
        switch icon {
        case "clear-day":
            image = "01d"
        case "clear-night":
            image = "01n"
        case "rain":
            image = "09d"
        case "snow":
            image = "13d"
        case "sleet":
            image = "13d"
        case "wind":
            image = "03d"
        case "fog":
            image = "50d"
        case "cloudy":
            image = "04d"
        case "partly-cloudy-day":
            image = "02d"
        case "partly-cloudy-night":
            image = "02n"
        default:
            image = "50d"
        }
        
        return "https://openweathermap.org/img/w/\(image).png"
    }
}
